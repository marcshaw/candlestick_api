require 'rails_helper'
require 'shared_examples/validations'

describe Candlestick::Validator do
  describe '#valid?' do
    subject(:validator) { Candlestick::Validator.new }

    before do
      validator.assign_attributes(candlestick_data)
    end

    let(:candlestick_data) do
      {
        :pair => pair,
        :start_at => start_at,
        :end_at => end_at,
        :closing_price => closing_price,
        :opening_price => opening_price,
        :highest_price => highest_price,
        :lowest_price => lowest_price,
        :volume => volume,
        :number_of_trades => number_of_trades,
        :kline_interval => kline_interval
      }
    end

    let(:pair) { 'NEOBTC' }
    let(:start_at) { DateTime.now }
    let(:end_at) { start_at + 5.minutes }
    let(:closing_price) { 0.22 }
    let(:opening_price) { 0.43 }
    let(:highest_price) { 0.46 }
    let(:lowest_price) { 0.21 }
    let(:volume) { 13 }
    let(:number_of_trades) { 1433 }
    let(:kline_interval) { 15 }

    context 'when the validator is valid' do
      it 'returns valid' do
        expect(validator).to be_valid
      end
    end

    context 'when the validator is invalid' do
      context 'when pair is invalid' do
        let(:attribute) { :pair }

        context 'when it is not present' do
          let(:pair) { nil }
          include_examples "when it is not present"
        end
      end

      context 'when start_at is invalid' do
        let(:attribute) { :start_at }

        context 'when it is not present' do
          let(:start_at) { nil }
          let(:end_at) { DateTime.now }

          include_examples "when it is not present"
        end
      end

      context 'when end_at is invalid' do
        let(:attribute) { :end_at }

        context 'when it is not present' do
          let(:end_at) { nil }

          include_examples "when it is not present"
        end
      end

      context 'when closing_price is invalid' do
        let(:attribute) { :closing_price }

        context 'when it is not present' do
          let(:closing_price) { nil }

          include_examples "when it is not present"
        end
      end

      context 'when opening_price is invalid' do
        let(:attribute) { :opening_price }

        context 'when it is not present' do
          let(:opening_price) { nil }

          include_examples "when it is not present"
        end
      end

      context 'when highest_price is invalid' do
        let(:attribute) { :highest_price }

        context 'when it is not present' do
          let(:highest_price) { nil }

          include_examples "when it is not present"
        end
      end

      context 'when lowest_price is invalid' do
        let(:attribute) { :lowest_price }

        context 'when it is not present' do
          let(:lowest_price) { nil }

          include_examples "when it is not present"
        end
      end

      context 'when volume is invalid' do
        let(:attribute) { :volume }

        context 'when it is not present' do
          let(:volume) { nil }

          include_examples "when it is not present"
        end
      end

      context 'when number_of_trades is invalid' do
        let(:attribute) { :number_of_trades }

        context 'when it is not present' do
          let(:number_of_trades) { nil }

          include_examples "when it is not present"
        end
      end

      context 'when kline_interval is invalid' do
        let(:attribute) { :kline_interval }

        context 'when it is not present' do
          let(:kline_interval) { nil }

          include_examples "when it is not present"
        end
      end
    end
  end
end
