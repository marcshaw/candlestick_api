shared_examples 'when it is not present' do
  it 'is not valid' do
    expect(validator).to_not be_valid
    expect(validator.errors[attribute]).to include /can't be blank/
  end
end

