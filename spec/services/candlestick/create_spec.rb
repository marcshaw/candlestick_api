require 'rails_helper'

describe Candlestick::Create do
  subject(:created_candlestick) { Candlestick::Create.new(params: params, validator: validator).call }
  let(:validator) { Candlestick::Validator.new }

  describe '#call' do
    context 'when given valid params' do
      let(:params) { attributes_for(:valid_candlestick) }

      it 'saves a candlestick' do
        expect { created_candlestick }.to change { Candlestick.count }
          .by(1)

        expect(created_candlestick).to be_persisted
        expect(created_candlestick).to have_attributes(params)
      end
    end

    context 'when given invalid params' do
      let(:params) { attributes_for(:invalid_candlestick) }

      it 'returns false' do
        expect(created_candlestick).to be_falsey
      end
    end
  end
end
