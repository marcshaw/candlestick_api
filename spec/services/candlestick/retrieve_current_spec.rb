require 'rails_helper'

describe Candlestick::RetrieveCurrent do
  subject(:current_candlesticks) { Candlestick::RetrieveCurrent.new(kline_interval: kline_interval, pair: pair).call }
  let(:kline_interval) { "15m" }
  let(:pair) { 'BTCUSDT' }

  let(:binance_args) do
    {
      kline_interval: Binance::KlineIntervals::INTERVALS[kline_interval],
      pair: pair
    }
  end

  let(:binance_result) { 'binance_result' }

  describe '#call' do
    it 'calls the expected classes and returns the results' do
      expect(Binance::Candlestick::RetrieveCurrent).to receive(:new).with(binance_args).and_return(-> { binance_result } )
      expect(current_candlesticks).to eq [binance_result]
    end
  end
end
