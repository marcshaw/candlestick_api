require 'rails_helper'

describe Binance::Candlestick::RetrieveCurrent do
  subject(:current_candlesticks) { Binance::Candlestick::RetrieveCurrent.new(kline_interval: kline_interval, pair: pair).call }

  let(:kline_interval) { Binance::KlineIntervals::KLINE_15_MINUTE }
  let(:pair) { "Testpair" }

  context '#call' do
    context 'when we have the latest candlestick data' do
      let!(:pair_candlestick) { create(:valid_candlestick, :pair => pair, :end_at => Time.now) }

      before do
        create(:valid_candlestick, :pair => 'NotTestpair')
      end

      it 'returns the data' do
        expect(current_candlesticks).to eq pair_candlestick
      end
    end

    context 'when we do not have the latest candlestick data' do
      let!(:previous_candlestick) { create(:valid_candlestick, :pair => pair, :end_at => Time.now - 2.days) }
      let(:new_candlestick_data) { attributes_for(:valid_candlestick).with_indifferent_access }

      it 'returns the data' do
        expect(Binance::Candlestick::FetchLatest).to receive(:new).with(:kline_interval => kline_interval, :pair => pair, :limit => 1).and_return( -> { new_candlestick_data } )
        expect { current_candlesticks }.to change { Candlestick.count }.by(1)
        expect(current_candlesticks.attributes).to include new_candlestick_data
      end
    end

    context 'when we do not have any candlestick data' do
      let(:candlestick_data) { attributes_for(:valid_candlestick).with_indifferent_access }

      before do
        Candlestick.delete_all
      end

      it 'queries for the latest data, stores it, then returns it' do
        expect(Binance::Candlestick::FetchLatest).to receive(:new).with(:kline_interval => kline_interval, :pair => pair, :limit => 1).and_return( -> { candlestick_data } )
        expect { current_candlesticks }.to change { Candlestick.count }.from(0).to(1)
        expect(current_candlesticks.attributes).to include candlestick_data
      end
    end
  end
end
