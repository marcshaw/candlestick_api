require 'rails_helper'

describe Binance::Candlestick::FetchLatest do
  context '#call' do
    subject(:fetch_candlesticks) do
      Binance::Candlestick::FetchLatest.new(
        client: client,
        kline_interval: kline_interval,
        pair: pair,
        limit: limit
      )
    end

    let(:client) { double }
    let(:kline_interval) { Binance::KlineIntervals::KLINE_15_MINUTE }
    let(:pair) { 'NEOBTC' }
    let(:fake_data) { double }
    let(:limit) { 1 }

    it 'calls the kline method and returns the result' do
      expect(client).to receive(:klines).with(
        {
          pair: pair,
          interval: kline_interval.value,
          limit: limit
        }).and_return(fake_data)

      expect(fetch_candlesticks.call).to eq(fake_data)
    end
  end
end
