require 'rails_helper'

describe CandlestickSerializer do
  subject(:serializer) { CandlestickSerializer.new(candlestick) }

  let(:candlestick) { create(:valid_candlestick) }
  let(:expected_attributes) { candlestick.attributes.except(*ignored_attributes).with_indifferent_access }
  let(:serializer_attributes) { serializer.serializable_hash[:data][:attributes].with_indifferent_access }

  let(:ignored_attributes) do
    ["updated_at", "created_at", "id"]
  end

  it 'contains all the attributes bar the ignored ones' do
    expect(serializer_attributes).to eq(expected_attributes)
  end
end
