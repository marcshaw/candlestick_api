require 'rails_helper'

describe V1::CandlesticksController, type: :controller do
  describe 'get index' do
    subject(:index_request) { get :index, params: { kline_interval: kline_interval, pair: pair } }
    let(:kline_interval) { '15m' }
    let(:pair) { 'BTCUSDT' }


    context 'when given valid params' do
      let(:candlesticks) { [create(:valid_candlestick), create(:valid_candlestick)] }
      let(:candlesticks_json) { CandlestickSerializer.new(candlesticks).serialized_json }

      before do
        expect(Candlestick::RetrieveCurrent).
          to receive(:new).
          with(kline_interval: kline_interval, pair: pair).
          and_return(-> { candlesticks } )

        index_request
      end

      it 'returns the expected json body' do
        expect(response.body).to eq(candlesticks_json)
      end
    end

    context 'when given an invalid kline interval' do
      let(:kline_interval) { 'prob not valid eh' }

      before do
        index_request
      end

      it 'returns a bad request status code' do
        expect(response).to have_http_status(:bad_request)
      end
    end

    context 'when given an invalid pair' do
      let(:pair) { 'prob not valid eh' }

      before do
        index_request
      end

      it 'returns a bad request status code' do
        expect(response).to have_http_status(:bad_request)
      end
    end
  end
end
