require 'rails_helper'

describe Candlestick::SanitizeKlineInterval do
  subject(:sanitized_kline_interval) { Candlestick::SanitizeKlineInterval.new(kline_interval).call }

  describe '#call' do
    context 'when the interval is valid' do
      KlineInterval::VALID_INTERVAL_VALUES.each do |interval|
        let(:kline_interval) { interval }

        it 'returns the kline interval' do
          expect(sanitized_kline_interval).to eq kline_interval
        end
      end
    end

    context 'when the interval is invalid' do
      context 'when the interval is nil' do
        let(:kline_interval) { nil }

        it 'returns the kline interval' do
          expect(sanitized_kline_interval).to eq nil
        end
      end

      context 'when the interval is a string' do
        let(:kline_interval) { 'this should not be valid' }

        it 'returns the kline interval' do
          expect(sanitized_kline_interval).to eq nil
        end
      end
    end
  end
end
