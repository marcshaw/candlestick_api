require 'rails_helper'

describe Candlestick::SanitizePair do
  subject(:sanitized_pair) { Candlestick::SanitizePair.new(pair).call }

  describe '#call' do
    context 'when the pair is valid' do
      Pair::VALID_PAIR_NAMES.each do |pair_name|
        let(:pair) { pair_name }

        it 'returns the pair' do
          expect(sanitized_pair).to eq pair
        end
      end
    end

    context 'when the pair is invalid' do
      context 'when the pair is nil' do
        let(:pair) { nil }

        it 'returns the pair' do
          expect(sanitized_pair).to eq nil
        end
      end

      context 'when the pair is a string' do
        let(:pair) { 'this should not be valid' }

        it 'returns the pair' do
          expect(sanitized_pair).to eq nil
        end
      end
    end
  end
end
