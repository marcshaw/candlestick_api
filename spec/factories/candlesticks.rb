FactoryBot.define do
  factory :candlestick do
    factory :valid_candlestick do
      pair 'SYM'
      start_at Time.now
      end_at Time.now + 5.minutes
      opening_price 4.0
      closing_price 5.0
      highest_price 6.0
      lowest_price 3.0
      volume 2.0
      number_of_trades 100
      kline_interval 15
    end

    factory :invalid_candlestick do
      pair 'SYM'
    end
  end
end
