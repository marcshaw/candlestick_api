class Pair
  attr_reader :name

  VALID_PAIR_NAMES = ['BTCUSDT', 'NEOBTC', 'ADABTC']

  def initialize(name:)
    @name = name
  end
end
