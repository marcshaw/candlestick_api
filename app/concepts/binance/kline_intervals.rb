class Binance::KlineIntervals
  KLINE_15_MINUTE = KlineInterval.new(value: '15', minutes: 15.minutes )

  INTERVALS = { '15m' => KLINE_15_MINUTE }.freeze
end
