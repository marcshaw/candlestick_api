class KlineInterval
  attr_reader :value, :minutes

  VALID_INTERVAL_VALUES = ['15m', '30m']

  def initialize(value:, minutes:)
    @value = value
    @minutes = minutes
  end
end
