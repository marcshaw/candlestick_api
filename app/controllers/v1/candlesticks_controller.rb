class V1::CandlesticksController < ApplicationController
  def index
    if sanitized_kline_interval && sanitized_pair
      render json: CandlestickSerializer.new(current_candlesticks).serialized_json
    else
      head :bad_request
    end
  end

  private

  def current_candlesticks
    Candlestick::RetrieveCurrent.new(kline_interval: sanitized_kline_interval, pair: sanitized_pair).call
  end

  def sanitized_kline_interval
    @sanitized_kline_interval ||= Candlestick::SanitizeKlineInterval.new(params[:kline_interval]).call
  end

  def sanitized_pair
    @sanitized_pair ||= Candlestick::SanitizePair.new(params[:pair]).call
  end
end
