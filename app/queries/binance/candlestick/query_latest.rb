class Binance::Candlestick::QueryLatest
  def initialize(kline_interval:, pair:)
    @kline_interval = kline_interval
    @pair = pair
  end

  def call
    Candlestick.where(kline_interval: kline_interval.value, pair: pair).order(end_at: :desc).limit(1).first
  end

  private
  attr_reader :kline_interval, :pair
end
