class Candlestick::SanitizeKlineInterval
  def initialize(kline_interval)
    @kline_interval = kline_interval
  end

  def call
    KlineInterval::VALID_INTERVAL_VALUES.include?(kline_interval) ? kline_interval : nil
  end

  private
  attr_reader :kline_interval
end
