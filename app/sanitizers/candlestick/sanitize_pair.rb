class Candlestick::SanitizePair
  def initialize(pair)
    @pair = pair
  end

  def call
    Pair::VALID_PAIR_NAMES.include?(pair&.upcase) ? pair.upcase : nil
  end

  private
  attr_reader :pair
end
