class Candlestick::Validator
  include ActiveModel::AttributeAssignment
  include ActiveModel::Validations

  attr_accessor(
    :pair,
    :start_at,
    :end_at,
    :closing_price,
    :opening_price,
    :highest_price,
    :lowest_price,
    :volume,
    :number_of_trades,
    :kline_interval
  )

  validates :pair, presence: true
  validates :start_at, presence: true
  validates :end_at, presence: true
  validates :closing_price, presence: true
  validates :opening_price, presence: true
  validates :highest_price, presence: true
  validates :lowest_price, presence: true
  validates :volume, presence: true
  validates :number_of_trades, presence: true
  validates :kline_interval, presence: true
end
