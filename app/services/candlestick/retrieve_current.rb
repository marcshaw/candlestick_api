class Candlestick::RetrieveCurrent
  def initialize(kline_interval:, pair:)
    @kline_interval = kline_interval
    @pair = pair
  end

  def call
    [
      Binance::Candlestick::RetrieveCurrent.new(kline_interval: Binance::KlineIntervals::INTERVALS[kline_interval], pair: pair).call
    ]
  end

  private
  attr_reader :kline_interval, :pair
end
