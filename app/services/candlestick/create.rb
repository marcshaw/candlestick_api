class Candlestick::Create
  def initialize(params:, validator:)
    @params = params
    @validator = assign_validator_attributes(validator, params)
  end

  def call
    Candlestick.create!(params) if validator.valid?
  end

  private
  attr_reader :params, :validator

  def assign_validator_attributes(empty_validator, params)
    empty_validator.assign_attributes(params)
    empty_validator
  end
end
