class Binance::Candlestick::FetchLatest
  def initialize(client: Binance::Client::REST.new, kline_interval:, pair:, limit:)
    @client = client
    @kline_interval = kline_interval
    @pair = pair
    @limit = limit
  end

  def call
    client.klines pair: pair, interval: kline_interval.value, limit: limit
  end

  private
  attr_reader :client, :kline_interval,  :pair, :limit
end
