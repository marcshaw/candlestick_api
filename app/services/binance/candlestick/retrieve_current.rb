class Binance::Candlestick::RetrieveCurrent
  def initialize(kline_interval:, pair:)
    @kline_interval = kline_interval
    @pair = pair
  end

  def call
    candlestick = Binance::Candlestick::QueryLatest.new(kline_interval: kline_interval, pair: pair).call

    if candlestick.present? && latest?(candlestick)
      candlestick
    else
      raw_data = Binance::Candlestick::FetchLatest.new(kline_interval: kline_interval, pair: pair, limit: 1).call
      Candlestick::Create.new(params: raw_data, validator: Binance::Candlestick::Validator.new).call
    end
  end

  private
  attr_reader :kline_interval, :pair

  def latest?(candlestick)
    candlestick.end_at + kline_interval.minutes > Time.now
  end
end
