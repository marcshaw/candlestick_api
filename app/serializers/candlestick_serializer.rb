class CandlestickSerializer
  include FastJsonapi::ObjectSerializer
  set_type :candlestick

  attributes :pair, :start_at, :end_at, :opening_price, :closing_price, :highest_price, :lowest_price, :volume, :number_of_trades, :kline_interval
end
