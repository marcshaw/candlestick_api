# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180331111734) do

  create_table "candlesticks", force: :cascade do |t|
    t.text "pair", limit: 12, null: false
    t.datetime "start_at", null: false
    t.datetime "end_at", null: false
    t.decimal "opening_price", precision: 25, scale: 10, null: false
    t.decimal "closing_price", precision: 25, scale: 10, null: false
    t.decimal "highest_price", precision: 25, scale: 10, null: false
    t.decimal "lowest_price", precision: 25, scale: 10, null: false
    t.decimal "volume", precision: 25, scale: 4, null: false
    t.integer "number_of_trades", null: false
    t.integer "kline_interval", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
