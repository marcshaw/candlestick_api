class CreateCandlesticks < ActiveRecord::Migration[5.1]
  def change
    create_table :candlesticks do |t|
      t.text :symbol, limit: 12
      t.datetime :start_at
      t.datetime :end_at
      t.decimal :opening_price, :precision => 25, :scale => 10
      t.decimal :closing_price, :precision => 25, :scale => 10
      t.decimal :highest_price, :precision => 25, :scale => 10
      t.decimal :lowest_price, :precision => 25, :scale => 10
      t.decimal :volume, :precision => 25, :scale => 4
      t.integer :number_of_trades
      t.integer :kline_interval
      t.timestamps
    end
  end
end
