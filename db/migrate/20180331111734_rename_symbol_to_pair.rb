class RenameSymbolToPair < ActiveRecord::Migration[5.1]
  def change
    rename_column :candlesticks, :symbol, :pair
  end
end
